class Costurero {
	method puedeTrabajarSobre(uniforme) = uniforme.soportaCosturaExtra()	
	method pasoUniforme(uniforme) { uniforme.agregarCosturaExtra() }
}

class Bordador { 
	var _puedeSacarLeyenda
	
	method puedeTrabajarSobre(uniforme) = _puedeSacarLeyenda or !uniforme.tieneLeyenda()	
	method pasoUniforme(uniforme) { 
		uniforme.ponerLeyenda(uniforme.cliente().nombreEmpresa())
	}
}